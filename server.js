const express = require('express');
const https = require('https');
const fs = require('fs');
const app = express();
const port = 8000;


const options = {
  key: fs.readFileSync('/home/xiteio/tentive.wnets.net/cert/tentive.wnets.net.key'),
  cert: fs.readFileSync('/home/xiteio/tentive.wnets.net/cert/tentive.wnets.net.crt')
};

// Set public folder as root
var activeSockets = [];
app.use(express.static('public'));

// Provide access to node_modules folder from the client-side
app.use('/scripts', express.static(`${__dirname}/node_modules/`));

// Redirect all traffic to index.html
app.use((req, res) => res.sendFile(`${__dirname}/public/index.html`));

// app.listen(port, () => {
//   console.info('listening on %d', port);
// });
// app.listen(8000);

app.use((req, res) => {
  res.writeHead(200);
  res.end("hello world\n");
});

var server = https.createServer(options,app);
var io = require('socket.io').listen(server.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
}));

io.of("/chats").on('connection', socket => {

  var users=socket.handshake.query.foo; 

  console.log(users,"users");


 // socket.on("join", roomid => {
    
      // socket.join(roomid);
      // manager.to(roomid).emit('testsocket',roomid);
     

      const existingSocket = activeSockets.find(
        existingSocket => existingSocket.users === users
      );
      console.log(activeSockets,"this.activeSockets");

      console.log(existingSocket,"existingSocket");

     if (!existingSocket) {
     
        console.log(existingSocket,"existingSocket");

        activeSockets.push({socketId:socket.id,users:users});

        socket.emit("update-user-list", { 

          users: activeSockets.filter(
            existingSocket => existingSocket.users !== users
          )
        });

        socket.broadcast.emit("update-user-list", {
          users: [{socketId:socket.id,users:users}]
        });

        console.log("existingSocket");


      }
    // this.activeSockets.push({socketId:socket.id,users:users});

    // socket.broadcast.emit("update-user-list", {
    //        users: [{socketId:socket.id,users:users}]
    //     });


    socket.on("share", (data) => {
      console.log("sharer***********",data);
      socket.to(data.to).emit("openshare", {
         socket: socket.id
      });
    });

    



      socket.on("call-user", (data) => {
        console.log("call-user***********",data.room);
        socket.to(data.to).emit("call-made", {
          offer: data.offer,
          socket: socket.id,
          room: data.room
        });
      });

      socket.on("call-user1", (data) => {
        console.log("call-user1s#############",data);
        socket.to(data.to).emit("call-made", {
          offer: data.offer,
          socket: socket.id,
          type:"share"
        });
        
      });



      socket.on("make-answer", data => {
        console.log("make-answer",data);
        socket.to(data.to).emit("answer-made", {
          socket: socket.id,
          answer: data.answer
        });
      });

      // socket.on("reject-call", data => {
      //   console.log("reject-call",data); 
      //   socket.to(data.from).emit("call-rejected", {
      //     socket: socket.id
      //   });
      // });

      socket.on("disconnect", () => {
        console.log("disconnect"); 
        activeSockets = activeSockets.filter(
          existingSocket => existingSocket.users !== users
        );


        
        socket.broadcast.emit("remove-user", {
          socketId: socket.id
        });
      });


  //  })

 
})


// http.createServer( app).listen(8000);