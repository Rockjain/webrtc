
var UniqueRoom = ''
window.addEventListener('load', () => {
  // Chat platform
  const chatTemplate = Handlebars.compile($('#chat-template').html());
  const chatContentTemplate = Handlebars.compile($('#chat-content-template').html());
  const chatEl = $('#chat');
  const formEl = $('.form');
  const messages = [];
  let username;

  // Local Video
  // const localImageEl = $('#local-image');
  // const localVideoEl = $('#local-video');

  // Remote Videos
  // const remoteVideoTemplate = Handlebars.compile($('#remote-video-template').html());
  // const remoteVideosEl = $('#remote-video');
  let remoteVideosCount = 0;

  // Hide cameras until they are initialized
  // localVideoEl.hide();

  // Add validation rules to Create/Join Room Form
  formEl.form({
    fields: {
      roomName: 'empty',
      username: 'empty',
    },
  });

  // create our webrtc connection
  const webrtc = new SimpleWebRTC({
    // the id/element dom element that will hold "our" video
    // localVideoEl: 'local-video',
    // the id/element dom element that will hold remote videos
    // remoteVideosEl: 'remote-video',
    // immediately ask for camera access
    autoRequestMedia: true,
    debug: false,
    detectSpeakingEvents: true,
    autoAdjustMic: false,
  });

  // We got access to local camera
  webrtc.on('localStream', () => {
    localImageEl.hide();
    // localVideoEl.show();
  });

  // Remote video was added
  // webrtc.on('videoAdded', (video, peer) => {
  //   // eslint-disable-next-line no-console
  //   const id = webrtc.getDomId(peer);
  //   const html = remoteVideoTemplate({ id });
  //   if (remoteVideosCount === 0) {
  //     remoteVideosEl.html(html);
  //   } else {
  //     remoteVideosEl.append(html);
  //   }
  //   $(`#${id}`).html(video);
  //   $(`#${id} video`).addClass('ui image medium'); // Make video element responsive
  //   remoteVideosCount += 1;
  // });

  // Update Chat Messages
  const updateChatMessages = () => {
    const html = chatContentTemplate({ messages });
    const chatContentEl = $('#chat-content');
    chatContentEl.html(html);
    // automatically scroll downwards
    const scrollHeight = chatContentEl.prop('scrollHeight');
    chatContentEl.animate({ scrollTop: scrollHeight }, 'slow');
  };

  // Post Local Message
  const postMessage = (message) => {
    const chatMessage = {
      username,
      message,
      postedOn: new Date().toLocaleString('en-GB'),
    };
    // Send to all peers
    webrtc.sendToAll('chat', chatMessage);
    // Update messages locally
    messages.push(chatMessage);
    $('#post-message').val('');
    updateChatMessages();
  };

  // Display Chat Interface
  const showChatRoom = (room) => {
    formEl.hide();
    const html = chatTemplate({ room });
    chatEl.html(html);
    const postForm = $('form');
    postForm.form({
      message: 'empty',
    });
    $('#post-btn').on('click', () => {
      const message = $('#post-message').val();
      postMessage(message);
    });
    $('#post-message').on('keyup', (event) => {
      if (event.keyCode === 13) {
        const message = $('#post-message').val();
        postMessage(message);
      }
    });
  };

  // Register new Chat Room
  const createRoom = (roomName) => {
    // eslint-disable-next-line no-console
    console.info(`Creating new room: ${roomName}`);
    webrtc.createRoom(roomName, (err, name) => {
      formEl.form('clear');
      showChatRoom(name);
      UniqueRoom= roomName
      postMessage(`${username} created chatroom`);
    });
  };

  // Join existing Chat Room
  const joinRoom = (roomName) => {
    // eslint-disable-next-line no-console
    console.log(`Joining Room: ${roomName}`);
    UniqueRoom= roomName
    webrtc.joinRoom(roomName);
    showChatRoom(roomName);
    postMessage(`${username} joined chatroom`);
  };

  // Receive message from remote user
  webrtc.connection.on('message', (data) => {
    if (data.type === 'chat') {
      const message = data.payload;
      messages.push(message);
      updateChatMessages();
    }
  });

  // Room Submit Button Handler
  $('.submit').on('click', (event) => {
    if (!formEl.form('is valid')) {
      return false;
    }
    username = $('#username').val();
    const roomName = $('#roomName').val().toLowerCase();
    if (event.target.id === 'create-btn') {
      createRoom(roomName);
      // alert(roomName)
    } else {
      joinRoom(roomName);
    }
    return false;
  });


let isAlreadyCalling = false;
let getCalled = false;
let loggedUser = "";

const existingCalls = [];

const { RTCPeerConnection, RTCSessionDescription } = window;

const peerConnection = new RTCPeerConnection({
  iceServers: [
    {
      urls: "turn:numb.viagenie.ca",
      username: "krishna.forebear@gmail.com",
      credential: "krishna"
    }
  
  ]
});

function unselectUsersFromList() {
  const alreadySelectedUser = document.querySelectorAll(
    ".active-user.active-user--selected"
  );

  alreadySelectedUser.forEach(el => {
    el.setAttribute("class", "active-user");
  });
}

function createUserItemContainer(socketId) {
  const userContainerEl = document.createElement("div");

  const usernameEl = document.createElement("p");

  userContainerEl.setAttribute("class", "active-user");
  userContainerEl.setAttribute("id", socketId);
  usernameEl.setAttribute("class", "username");
  usernameEl.innerHTML = `Socket: ${socketId}`;

  userContainerEl.appendChild(usernameEl);

  userContainerEl.addEventListener("click", () => {
    unselectUsersFromList();
    userContainerEl.setAttribute("class", "active-user active-user--selected");
    const talkingWithInfo = document.getElementById("talking-with-info");
    talkingWithInfo.innerHTML = `Talking with: "Socket: ${socketId}"`;
    callUser(socketId);
  });

  return userContainerEl;
}

const userContainerElN = document.getElementById("sessionbtn");
userContainerElN.addEventListener("click", () => {
  userContainerElN.textContent;
  alert($("#sessionbtn").data("myval"));
  //data-sessionID
  callUser($("#sessionbtn").data("myval"));
});

async function callUser(socketId,roomName) {
  const offer = await peerConnection.createOffer();
  await peerConnection.setLocalDescription(new RTCSessionDescription(offer));
  console.log("room", UniqueRoom)
  socket.emit("call-user", {
    offer,
    to: socketId,
    room: UniqueRoom
  });
}

async function callUser1(socketId) {
  const offer = await peerConnection.createOffer();
  await peerConnection.setLocalDescription(new RTCSessionDescription(offer));

  socket.emit("call-user1", {
    offer,
    to: socketId
  });
}

function updateUserList(socketIds) {
  const activeUserContainer = document.getElementById("active-user-container");

  socketIds.forEach(socketId => {
    const alreadyExistingUser = document.getElementById(socketId.socketId);
    if (!alreadyExistingUser) {
      const userContainerEl = createUserItemContainer(socketId.socketId);

      activeUserContainer.appendChild(userContainerEl);
    }
  });
}

// const socket = io.connect("http://localhost:5000/chats");
// var room = "abc123";
// socket.emit('room', room);
// socket.on('message', function(data) {
// if(room==data){
//   console.log('Incoming message:', data);
// }
// });

var urlParams = new URLSearchParams(window.location.search);

// alert(urlParams.get("foo"));

var username1 = urlParams.get("foo");
var username_trainee = "user";

function updateUserfromList(socketIds) {
  socketIds.forEach(socketId => {
    if (username_trainee == socketId.users) {
      $("#sessionbtn").data("myval", socketId.socketId);
      $("#sessionbtn").text(username_trainee);
    }
    if ("user1" == socketId.users) {
      $("#share").data("myval1", socketId.socketId);
      $("#share").text("share screen");
    }
  });
}

var socket = io.connect("https://tentive.wnets.net:8000/chats", {
  query: "foo=" + username1
});          

// var roomID = "tesr123" //Should ideally be got from req.params.roomid
// socket.emit("join", roomID)
// socket.on('testsocket', function (data) {
//     console.log("Connected to room", data);
// });

socket.on("openshare", ({ users }) => {
  // getMedia();
  addScreen();
});

socket.on("update-user-list", ({ users }) => {
  console.log(users);
  updateUserList(users);
  updateUserfromList(users);
});

socket.on("remove-user", ({ socketId }) => {
  const elToRemove = document.getElementById(socketId);

  if (elToRemove) {
    elToRemove.remove();
  }
});

socket.on("call-made", async data => {
  console.log("call-made1", data);

  //alert(getCalled);
    if(data.room != UniqueRoom){
      // alert(UniqueRoom)
       return   
    }
  if (getCalled) {
    // if(data.type=="share"){

    console.log(data.type);

    // await getMedia();
    // await  addScreen();

    // }
    //  all();

    if (!isAlreadyCalling) {
      const confirmed = confirm(
        `User "Socket: ${data.socket}" wants to call you. Do accept this call?`
      );

      if (!confirmed) {
        socket.emit("reject-call", {
          from: data.socket
        });

        return;
      }
    }
  }

  await peerConnection.setRemoteDescription(
    new RTCSessionDescription(data.offer)
  );
  const answer = await peerConnection.createAnswer();
  await peerConnection.setLocalDescription(new RTCSessionDescription(answer));

  socket.emit("make-answer", {
    answer,
    to: data.socket
  });
  getCalled = true;
});

socket.on("answer-made", async data => {
  console.log("answer-made", data);
  await peerConnection.setRemoteDescription(
    new RTCSessionDescription(data.answer)
  );

  if (!isAlreadyCalling) {
    callUser(data.socket);
    isAlreadyCalling = true;
  }
});

socket.on("call-rejected", data => {
  alert(`User: "Socket: ${data.socket}" rejected your call.`);
  unselectUsersFromList();
});

async function addScreen1() {
  peerConnection.ontrack = function(streams) {
    console.log(streams, "streams");

    const remoteVideo = document.getElementById("remote-video");
    if (remoteVideo) {
      remoteVideo.srcObject = streams.streams[0];
    }
  };
}

gotFirstMediaStream = false;

async function addScreen() {
  peerConnection.ontrack = function(streams) {
    console.log("streams");

    if (!gotFirstMediaStream) {
      gotFirstMediaStream = true;
      console.log("streams 1");
      const remoteVideo = document.getElementById("share-video");
      if (remoteVideo) {
        remoteVideo.srcObject = streams.streams[0];
      }
    } else {
      console.log("streams 2");

      const remoteVideo = document.getElementById("remote-video");
      if (remoteVideo) {
        remoteVideo.srcObject = streams.streams[0];
      }
    }
  };
}

async function getMedia1() {
  console.log("getMedia1");
  navigator.getUserMedia =
    navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia;
  navigator.getUserMedia(
    { video: true, audio: true },
    stream => {
      const localVideo = document.getElementById("local-video");
      if (localVideo) {
        localVideo.srcObject = stream;
      }
      console.log(stream.getTracks(), "whole1");
      stream
        .getTracks()
        .forEach(track => peerConnection.addTrack(track, stream));
    },
    error => {
      console.warn(error.message);
    }
  );
}

var displayMediaOptions = {
  video: true
};
var displayMediaOptions1 = { video: true, audio: true };

async function getMedia(displayMediaOptions) {
  //const localVideo = document.getElementById("share-video");

  stream = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
  //localVideo.srcObject = stream;

  console.log(stream.getTracks(), "whole");
  stream.getTracks().forEach(track => {
    track.isActive = true;
    peerConnection.addTrack(track, stream);

    //console.log(track,"track");
  });
}

async function all() {
  await getMedia1();
}

all();

addScreen1();
// addScreen();
//getMedia();

// getMedia1()
// getMedia();

//getMedia1()

const share = document.getElementById("share");
share.addEventListener("click", async () => {
  var socketId = $("#share").data("myval1");
  socket.emit("share", {
    to: socketId
  });
  await getMedia();
  addScreen();
  // all();

  // setTimeout(() => {

  callUser($("#share").data("myval1"));

  // },1000 )

  //$("share-video").hide();
});

// navigator.mediaDevices.getDisplayMedia(displayMediaOptions).then( stream => {
//   const localVideo = document.getElementById("local-video");
//   localVideo.srcObject = stream;
//   stream.getTracks().forEach(track => peerConnection.addTrack(track, stream));
// }

// )


});


